import java.util.ArrayList;
import java.util.List;

public class WeekOne {
	static int MAX_SIZE=1000;
	public static class SegmentationTree{
        public class SegTreeNode{
            int start;
            int end;
            int count;
            SegTreeNode left;
            SegTreeNode right;
            public SegTreeNode(int s,int e,int c){
                start=s;
                end=e;
                count=c;
                left=null;
                right=null;
            }
        }
        SegTreeNode root;
        public SegmentationTree(int start,int end){
            root=build(start,end);
        }
        private SegTreeNode build(int start,int end){
            if(start>end)return null;	
            if(start==end)return new SegTreeNode(start,end,0);
            int mid=start+(end-start)/2;
            SegTreeNode root=new SegTreeNode(start,end,0);
            root.left=build(start,mid);
            root.right=build(mid+1,end);
            return root;
        }
        public int query(int q_start,int q_end){
            return query(root,q_start,q_end);
        }
        private int query(SegTreeNode root,int q_start,int q_end){
            if(root==null||root.start>q_end||root.end<q_start)return 0;
            if(root.start>=q_start && root.end<=q_end)return root.count;
            return query(root.left,q_start,q_end)+query(root.right,q_start,q_end);
        }
        public void addOne(int index){
            addOne(root,index);
        }
        private void addOne(SegTreeNode root,int index){
            if(root.start==root.end){
                if(root.start==index)root.count++;
                return;
            }
            if(root.left!=null && root.left.end>=index)addOne(root.left,index);
            else addOne(root.right,index);
            int l=root.left!=null?root.left.count:0;
            int r=root.right!=null?root.right.count:0;
            root.count=l+r;
        }
    }
	public static void main(String[] args){
		//given an array, Count smaller elements on left side for each element
		int[] A={2,3,2,4,3};
		List<Integer> result=new ArrayList<Integer>();
        SegmentationTree mytree=new SegmentationTree(0,MAX_SIZE);
        for(int i=0;i<A.length;i++){
            result.add(mytree.query(0,A[i]-1));
            mytree.addOne(A[i]);
        }
        System.out.println(result);
	}
}
